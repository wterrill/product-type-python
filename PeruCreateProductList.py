import sys
print(sys.version)
import pandas as pd
import numpy as np
import math

def isnan(value):
  try:
      return math.isnan(float(value))
  except:
      return False

def make_Values(FileName,sheet,FinalName,version):
        df_master = pd.read_excel(FileName,sheet)
        df_master = df_master.drop_duplicates("SKU")
        df_master_for_producttypes = df_master
        

        # Let's iterate over columns to get the right shape
        startingColNames = df_master.columns.values.tolist()
        i=0
        for name in startingColNames:
                if name not in ['SKU', 'Product Type', 'Nombre', 'Descripcion']:
                        i=i+1
                        #get index of column
                        colIndex = df_master.columns.get_loc(name)
                        #get number of rows
                        rows = df_master.shape[0]
                        newColValue = [name] * rows
                        df_master = df_master.rename(columns={ df_master.columns[colIndex]: "Attribute "+str(i)+" Value" })
                        df_master.insert(colIndex, "Attribute " + str(i), newColValue, allow_duplicates=False)  
# Let´s verify the order of the columns and reposition them if needed
# For Products, the order should be: SKU,Nombre,Descripcion,Product Type,Attribute 1,Attribute 1 Value, etc...
# We´ll assume the attributes are fine... let´s focus on the first 4 columns
        cols = df_master.columns.tolist()
        cols.remove('Descripcion')
        cols.remove('SKU')
        cols.remove('Nombre')
        cols.remove('Product Type')
        cols = ['SKU','Nombre','Descripcion','Product Type'] + cols
        df_master = df_master[cols]


        # Get rid of the underscores in the product types
        df_master['Product Type'] = df_master['Product Type'].str.replace("_"," ")
        #This trims leading and trailing whitespace
        df_master['Product Type'] = df_master['Product Type'].str.replace('^[ \t]+', '', regex = True)
        df_master['Product Type'] = df_master['Product Type'].str.replace('[ \t]+$', '', regex = True)


        df_master.to_csv("PeruFiles/"+FinalName+version+"-SKU.csv", index=False)


       
 ########## Create product types document ###############################################################################################################
        df_sortedmaster = df_master_for_producttypes.sort_values('Product Type')
        allProductTypes = df_sortedmaster['Product Type'].unique()

        # iterate through product type to find out where it goes:


        #df_finalProductTypes = pd.DataFrame([], columns = df_master.columns.values.tolist()) 
        #df_finalProductTypes = df_finalProductTypes.drop("SKU", axis=1)
        df_finalProductTypes = pd.DataFrame([])


        for producttype in allProductTypes:
                print(producttype)
                df_someproduct = df_sortedmaster.loc[df_sortedmaster['Product Type'] == producttype]
                rowValues = {'Product Type' : producttype}
                for column in df_someproduct:
                        # Coll
                        if column not in ['SKU', 'Product Type', 'NOMBRE', 'DESCRIPCION-CORTA']:
                                values = df_someproduct[column].unique().tolist()
                                stringValues = ";".join(str(x) for x in values)
                                if column in ['EAN','Nombre','DENOMINACION-VARIEDAD','DESCRIPCION-TECNICA','GRASA-TOTAL-POR-PORCION-G','GRASA-TOTAL-VD-G','COMPONENTE-ACTIVO-NUTRICIONAL','DESCRIPCION-CORTA','DESCRIPCION-TECNICA','PORCION-RECONSTITUIDA','MODO-DE-PREPARACION','SUGERENCIAS-DE-PREPARACION-Y-CONSUMO','ADVERTENCIA-DE-PREPARACION-Y-CONSUMO','TEMPERATURA-DE-ALMACENAMIENTO','ADVERTENCIA-DE-ALMACENAMIENTO','EDAD-SUGERIDA-DE-CONSUMO','INGREDIENTES','DESCRIPTOR-NUTRICIONAL','DECLARACION-DE-PROPIEDADES-SALUDABLES','DECLARACION-DE-PROPIEDADES-NUTRICIONALES','ADVERTENCIAS-NUTRICIONALES','ENERGIA-POR-100-GR-KCAL','ENERGIA-POR-PORCION-KCAL','ENERGIA-VD-KCAL','ENERGIA-PORCION-RECONSTITUIDA','ENERGIA-POR-100-GR-RECONSTITUIDO-KCAL','PROTEINAS-POR-100-GR-G','PROTEINAS-POR-PORCION-G','PROTEINAS-VD-G','PROTEINAS-PORCION-RECONSTITUIDA','PROTEINAS-POR-100-GR-RECONSTITUIDO-G','GRASA-TOTAL-POR-100-GR-G','GRASA-TOTAL-POR-PORCION-GGRASA-TOTAL-VD-G','GRASA-TOTAL-POR-100-GR-RECONSTITUIDO-G','GRASAS-SATURADAS-POR-100-GR-G','GRASAS-SATURADAS-POR-PORCION-G','GRASAS-SATURADAS-VD-G','GRASAS-SATURADAS-PORCION-RECONSTITUIDA','GRASAS-SATURADAS-POR-100-GR-RECONSTITUIDO-G','GRASAS-MONOINSATURADAS-POR-100-G-G','GRASAS-MONOINSATURADAS-POR-PORCION-G','GRASAS-MONOINSATURADAS-VD-G','GRASAS-MONOINSATURADAS-PORCION-RECONSTITUIDA','GRASAS-MONIINSATURADAS-POR-100-GR-RECONSTITUIDO-G','GRASAS-POLIINSATURADAS-POR-100-G-G','GRASAS-POLIINSATURADAS-POR-PORCION-G','GRASAS-POLIINSATURADAS-VD-G','GRASAS-POLIINSATURADAS-PORCION-RECONSTITUIDA','GRASAS-POLIINSATURADAS-POR-100-GR-RECONSTITUIDO-G','GRASAS-TRANS-POR-100-GR-G','GRASAS-TRANS-POR-PORCION-G','GRASAS-TRANS-VD-G','GRASAS-TRANS-PORCION-RECONSTITUIDA','GRASAS-TRANS-POR-100-GR-RECONSTITUIDO-G','COLESTEROL-POR-100-GR-MG','COLESTEROL-POR-PORCION-MG','COLESTEROL-VD-MG','COLESTEROL-PORCION-RECONSTITUIDACOLESTEROL-POR-100-GR-RECONSTITUIDO-G','HIDRATOS-DE-CARBONO-DISPONIBLES-POR-100-GR-G','HIDRATOS-DE-CARBONO-DISPONIBLES-POR-PORCION-G','HIDRATOS-DE-CARBONO-DISPONIBLES-VD-G','HIDRATOS-DE-CARBONO-DISPONIBLES-POR-100-GR-RECONSTITUIDO-G','AZUCARES-TOTALES-POR-100-GR-G','AZUCARES-TOTALES-POR-PORCION-G','SODIO-POR-100-GR-MG','SODIO-POR-PORCION-MG','Instrucciones-de-uso','Beneficios-de-uso','Advertencias-de-uso']:
                                        stringValues = "TEXT"
                                if column in ['DECLARACION-SIN-ALCOHOL','ALTO-EN-AZUCAR','ALTO-EN-CALORIAS','ALTO-EN-SODIO','ALTO-EN-GRASAS','LIBRE-DE-AZUCAR-AnADIDA','CONTIENE-LACTOSA','CONTIENE-GLUTEN','CONTIENE-HUEVO-Y-DERIVADOS','CONTIENE-MANI-SOYA-Y-DERIVADOS','CONTIENE-NUECES','CONTIENE-PESCADOS','CONTIENE-MARISCOS','CONTIENE-SULFITOS','CONTIENE-OTROS-ALERGENOS','LIBRE-DE-COLORANTES-ARTIFICIALES','LIBRE-DE-PRESERVANTES-ARTIFICIALES','LIBRE-DE-SABORIZANTES-ARTIFICIALES','LIBRE-DE-GLUTEN','LIBRE-DE-LACTOSA','VEGANO','ORGANICO','FABRICACION-ARTESANAL','DECLARACION-SIN-ALCOHOL','ALTO-EN-AZUCAR','ALTO-EN-CALORIASALTO-EN-SODIO','ALTO-EN-GRASAS','LIBRE-DE-AZUCAR-AnADIDA','CONTIENE-LACTOSA','CONTIENE-GLUTEN','CONTIENE-HUEVO-Y-DERIVADOS','CONTIENE-MANI-SOYA-Y-DERIVADOS','CONTIENE-NUECES','CONTIENE-PESCADOS','CONTIENE-MARISCOS','CONTIENE-SULFITOS','CONTIENE-OTROS-ALERGENOS','LIBRE-DE-COLORANTES-ARTIFICIALES','LIBRE-DE-PRESERVANTES-ARTIFICIALES','LIBRE-DE-SABORIZANTES-ARTIFICIALES','LIBRE-DE-GLUTEN','LIBRE-DE-LACTOSA','VEGANO','ORGANICO','FABRICACION-ARTESANAL']:
                                        stringValues = "BOOL"
                                rowValues.update({column:stringValues})
                                

                cols = df_master.columns.values.tolist()
                cols.remove('SKU')
                df_temp = pd.DataFrame(rowValues, index=[0])
                df_finalProductTypes = df_finalProductTypes.append(df_temp, ignore_index=True)

        startingColNames = df_finalProductTypes.columns.values.tolist()
        i=0
        for name in startingColNames:
                if name not in ['SKU', 'Product Type']:
                        i=i+1
                        #get index of column
                        colIndex = df_finalProductTypes.columns.get_loc(name)
                        #get number of rows
                        rows = df_finalProductTypes.shape[0]
                        newColValue = [name] * rows
                        df_finalProductTypes = df_finalProductTypes.rename(columns={ df_finalProductTypes.columns[colIndex]: "Attribute "+str(i)+" Value" })
                        df_finalProductTypes.insert(colIndex, "Attribute " + str(i), newColValue, allow_duplicates=False)  


        # Make label and product_type
        colIndex = df_finalProductTypes.columns.get_loc("Attribute 1")
        product_types = df_finalProductTypes['Product Type'].tolist()
        labels = df_finalProductTypes['Product Type'].tolist()
        ls_hyphen_products=[]
        for product in product_types:
                if not isnan(product):
                        hyphen_product = product.replace(' ','_')
                        hyphen_product = hyphen_product.upper()
                        ls_hyphen_products.append(hyphen_product)
                else:
                        print("bollocks")

        ls_labels = []
        for label in labels:
                if not isnan(label):
                        i = label.lower()
                        j = i.title()
                        ls_labels.append(i)
                else:
                        print("bollocks")


        #df_master = df_master.rename(columns={ df_master.columns[colIndex]: "Attribute "+str(i)+" Value" })
        df_finalProductTypes.insert(colIndex, "Nombre", ls_labels, allow_duplicates=False)  
        df_finalProductTypes['Product Type'] = ls_hyphen_products
        df_finalProductTypes = df_finalProductTypes.replace(';nan','',regex=True)
        df_finalProductTypes = df_finalProductTypes.replace('nan;','',regex=True)
        df_finalProductTypes = df_finalProductTypes.replace(';nan;','',regex=True)
        df_finalProductTypes = df_finalProductTypes.replace('nan','',regex=False)


        #put in required and search
        #Let's iterate over columns to get the right shape
        startingColNames = df_finalProductTypes.columns.values.tolist()
        i=0
        for name in startingColNames:
                if name not in ['SKU', 'Product Type']:
                        if "Value" in name:
                                i=i+1
                                #get index of column
                                colIndex = df_finalProductTypes.columns.get_loc(name)
                                #get number of rows
                                rows = df_finalProductTypes.shape[0]
                                requiredVal = ["No"] * rows
                                searchable = ["Searchable"] * rows
                                df_finalProductTypes.insert(colIndex+1, "Attribute " + str(i) + " Searchable:combinationUnique:sets " , searchable, allow_duplicates=True)  
                                #df_finalProductTypes.insert(colIndex+1, "Attribute " + str(i) + " Required " , requiredVal, allow_duplicates=True) 

        #output to CSV
        # Get rid of the underscores in the product types
        df_finalProductTypes['Product Type'] = df_finalProductTypes['Product Type'].str.replace("_"," ")
        #This trims leading and trailing whitespace
        df_finalProductTypes['Product Type'] = df_finalProductTypes['Product Type'].str.replace('^[ \t]+', '', regex = True)
        df_finalProductTypes['Product Type'] = df_finalProductTypes['Product Type'].str.replace('[ \t]+$', '', regex = True)

        df_finalProductTypes.to_csv("PeruFiles/"+FinalName+version+"-PT.csv", index=False)


version = "_PERU_VER_6"
inputs = [
        #("PeruFiles/SURTIDO FOOD TOPE - FINAL.xlsx","FOOD","FOOD_FINAL",version),
        ("PeruFiles/TOPE - Product Types Non-Food final (1).xlsx","Sheet1","Nonfood",version),

]
for input in inputs:
        make_Values(input[0],input[1],input[2],input[3])