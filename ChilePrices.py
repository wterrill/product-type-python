import pandas as pd
import numpy as np

df = pd.read_excel('price_tocl.xlsx')

print(df)
df = df.sort_values('SKU_ID')
last_sku = ""
array = []
row_dict = {}
row_dict['list'] = ""
row_dict['sale'] = ""
row_dict['cost'] = ""
row_dict['sku'] = ""
first_time = True
for index, row in df.iterrows():
    sku = str(row['SKU_ID'])

    if sku != last_sku and not first_time:
        array.append(row_dict)
        row_dict = {}
        row_dict['list'] = ""
        row_dict['sale'] = ""
        row_dict['cost'] = ""
        row_dict['sku'] = ""
    
    row_dict['sku'] = sku
    if len(sku) == 6:
        row_dict['sku'] = "00" + sku
    if len(sku) == 7:
        row_dict['sku'] = "0" + sku
    price = row['LIST_PRICE']
    category = row['PRICE_LIST']
    #print(row['SKU_ID'], row['PRICE_LIST'], row['LIST_PRICE'])    
    if category == "costsPricesCL":
        row_dict["cost"] = row['LIST_PRICE']
    if category == "listPricesCL":
        row_dict["list"] = row['LIST_PRICE']
    if category == "salePricesCL":
        row_dict["sale"] = row['LIST_PRICE']
    last_sku = sku
    first_time = False

#print(array)
df_final = pd.DataFrame(array)
cols = df_final.columns.tolist()
cols = cols[-1:] + cols[:-1]
df_final = df_final[cols]

df_final.to_excel("prices_CL.xlsx", index=False)
