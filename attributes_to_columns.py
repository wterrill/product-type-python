import sys
print(sys.version)
import pandas as pd
import numpy as np
import math as math

def isnan(value):
  try:
      import math
      return math.isnan(float(value))
  except:
      return False

df_master = pd.read_excel("PeruFiles/TOPE - Product Types Non-Food final.xlsx","Nonfood")
df_master.columns = df_master.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

#Get all of the final column names
finalAttributes = []
for x in range(1, 24):
    print(x)
    attribute = "atribute_"+str(x)
    attribute_list = df_master[attribute].unique()
    for attribute in attribute_list:
        print(attribute)
        if not isnan(attribute):
            if '\n' in attribute:
                print(attribute)
        if attribute not in finalAttributes:
            if not isnan(attribute):
                finalAttributes.append(str(attribute))

finalAttributes.sort()
for attribute in finalAttributes:
    print(attribute)
print(finalAttributes)



# for x in range(1, 25):
#     attribute = "atribute_"+str(x)
#     attribute_list = df_master[attribute].unique()
#     for attribute in attribute_list:
#         if attribute not in finalAttributes:
#             if not isnan(attribute):
#                 print(attribute)

#Create final dataframe so that we can feed in that information
finalAttributes.insert(0, "marca")
finalAttributes.insert(0, "sku")
finalAttributes.insert(0, "product_type")
df_final = pd.DataFrame(index=range(len(df_master)),columns=range(len(finalAttributes)))
#df_final = pd.DataFrame(index = range(0), columns=range(len(finalAttributes)))      
df_final.columns = finalAttributes
print (df_final)
df_final = df_final.astype(str)

#iterate over rows and start to feed df_final
j=0
for index, row in df_master.iterrows():
    j=j+1
    # print("=====================================")
    print(j)
    # print("=====================================")
    # print(index)
    # rowValues =  [None] * len(finalAttributes)
    # df_temp = pd.DataFrame(rowValues, index=[0])
    # df_final.append(df_temp, ignore_index=True)
    #df_final = pd.DataFrame(index=range(len(df_master)),columns=range(len(finalAttributes)))
    #iterate over columns and build the rowValues dictionary
    startingColNames = df_master.columns.values.tolist()
    i=0
    dict_entry = "beer"
    dict_value = "beer2"
    i=0
    for name in startingColNames:
        i=i+1
        # print("*****")
        print("     "+str(i))
        # print("*****")
        # print(name)
        if "atribute" not in name: #This is the case for sku, marca, etc
            #print("first if")
            #rowValues.update({name:row[name]})
            try:
                df_final.at[index, name] = row[name]
            except:
                df_final = df_final.astype(str)
                df_final.at[index, name] = row[name]
        elif "v_" not in name: #
            #print("second elif")
            dict_entry = row[name]
        else:
            #print("third else")
            dict_value = row[name]
            if isnan(row[name]):
                dict_value = ""
            #print(name)
            if not isnan(dict_entry):
                df_final.at[index, dict_entry] = dict_value
            #rowValues.update({dict_entry:dict_value})
    # df_temp = pd.DataFrame(rowValues, index=[0])
    # df_final.append(df_temp, ignore_index=True)

    # print(row['product_type'], row['sku'])
    # print(index)

print(df_final)
FinalName = "Nonfood"
df_final.to_excel("PeruFiles/"+FinalName+"-PT.xlsx", encoding="utf-16", index=False)

# rowValues = {}
# rowValues.update({column:stringValues})
# df_temp = pd.DataFrame(rowValues, index=[0])
# df_finalProductTypes = df_finalProductTypes.append(df_temp, ignore_index=True)

# once through of created document
#df = pd.read_excel("PeruFiles/nnnon-food-PT.xlsx","J11")