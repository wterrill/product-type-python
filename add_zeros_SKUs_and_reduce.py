import pandas as pd
import numpy as np

version = "_VER_1"
desc = "_CHILE"

fileName = 'ChileFiles/prices_CL.xlsx'
sku_filename_food = 'ChileFiles/master.XLSX'
sku_filename_nonfood = 'ChileFiles/Fichas nonfood mayo2019.xlsx'
categories = 'sku_categorías_VER3_CHILE.xlsx - Exportar Hoja de Trabajo.xlsx'
cross = 'cross_will.csv'

df = pd.read_excel(fileName)
df = df.drop_duplicates("SKU")
df['SKU'] = df['SKU'].apply(lambda x: str(x).zfill(8))


df_sku_food = pd.read_excel(sku_filename_food)
df_sku_food = df_sku_food.drop_duplicates("SKU")
df_sku_food['SKU'] = df_sku_food['SKU'].apply(lambda x: str(x).zfill(8))


df_sku_nonfood = pd.read_excel(sku_filename_nonfood)
df_sku_nonfood = df_sku_nonfood.drop_duplicates("SKU")
df_sku_nonfood['SKU'] = df_sku_nonfood['SKU'].apply(lambda x: str(x).zfill(8))

df_sku_cat = pd.read_excel(categories)
df_sku_cat = df_sku_cat.drop_duplicates("SKU")
df_sku_cat['SKU'] = df_sku_cat['SKU'].apply(lambda x: str(x).zfill(8))

df_sku_X = pd.read_csv(cross)
df_sku_X = df_sku_X.drop_duplicates("SKU")
df_sku_X['SKU'] = df_sku_X['SKU'].apply(lambda x: str(x).zfill(8))

skuList = df_sku_food['SKU'].tolist() + df_sku_nonfood['SKU'].tolist()

print("beer")
df = df[df['SKU'].isin(skuList)]
print("Missing price values (in master, not in prices):", len((set(skuList).difference(df['SKU'].tolist()))))
temp_list = list(set(skuList).difference(df['SKU'].tolist()))
temp = pd.DataFrame(temp_list,columns = ['Missing SKUS'])
temp.to_csv("ChileFiles/Missing SKU Prices" + version + desc + ".csv", index=False)

df_sku_cat = df_sku_cat[df_sku_cat['SKU'].isin(skuList)]
print("Missing category values (in master, not in categories):", len((set(skuList).difference(df_sku_cat['SKU'].tolist()))))
temp_list = list(set(skuList).difference(df_sku_cat['SKU'].tolist()))
temp = pd.DataFrame(temp_list,columns = ['Missing SKUS'])
temp.to_csv("ChileFiles/Missing Category SKUs" + version + desc + ".csv", index=False)

df_sku_X = df_sku_X[df_sku_X['SKU'].isin(skuList)]
print("Missing Cross sell values (in master, not in cross sell):", len((set(skuList).difference(df_sku_X['SKU'].tolist()))))
temp_list = list(set(skuList).difference(df_sku_X['SKU'].tolist()))
temp = pd.DataFrame(temp_list,columns = ['Missing SKUS'])
temp.to_csv("ChileFiles/Missing cross SKUs" + version + desc + ".csv", index=False)

df.to_csv("ChileFiles/prices" + version + desc + ".csv", index=False)
df_sku_cat.to_csv("ChileFiles/categorías" + version + desc + ".csv", index=False)
df_sku_X.to_csv("ChileFiles/crossSell" + version + desc + ".csv", index=False)