import pandas as pd
import numpy as np

df_arbol = pd.read_excel('ChileFiles/arbol de categorias_VER3_CHILE.xlsx')
df_cat_sku = pd.read_csv('ChileFiles/categorías_VER_1_CHILE.csv')


#df_master = df_master.drop_duplicates("SKU")
categories=[]
for index, row in df_cat_sku.iterrows():
    beer = row['CATEGORIAS']
    try:
        beer = beer.replace(";","")
    except:
         beer = str(beer).replace(";","")
    wine = beer.split()
    for grape in wine:
        if grape not in categories:
            categories.append(grape)
        else:
            print(grape)

definedCategories = list(set(df_arbol['id-category1'])) + list(set(df_arbol[' id-category2'])) + list(set(df_arbol[' id-category3'])) + list(set(df_arbol[' id-category4'])) + list(set(df_arbol[' id-category5']))

troublemakers = []
for cat in categories:
    if cat not in definedCategories:
        troublemakers.append(cat)

troublemakers = list(set(troublemakers))
trouble = pd.DataFrame(troublemakers,columns=['undefined categories'])
trouble.to_csv("undefined categories.csv", index=False)